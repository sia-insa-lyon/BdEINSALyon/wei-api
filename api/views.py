import tablib
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters, status
from rest_framework.decorators import api_view, action, permission_classes
from rest_framework.decorators import permission_classes as permissions_required
from rest_framework.exceptions import PermissionDenied, NotAuthenticated
from rest_framework.generics import RetrieveAPIView, get_object_or_404
from rest_framework.response import Response

from api.serializers import *
from api.models import *
from account.permissions import *
from account.custom import refresh_token
# Create your views here.

class BizuthViewSet(viewsets.ModelViewSet):
    """
    Les champs Nom, Prénom, Email sont facultatifs, car ils seront remplis par une requête sur Adhésion graĉe
    au adhesion_id. Une fois remplis, ils sont mis dans la base de donnée, ce qui permet de les utiliser pour faire des recherches.
    Il suffit de les supprimer pour mettre à jour depuis Adhésion
    On peut aussi appeler /bizuths/{pk}/adhesion/ pour tout remettre à jour

    /bizuths/attente/ renvoie & actualise la liste d'attente

    /bizuths/readliste/ renvoie simplement la liste d'attente, mais elle n'est pas actualisée
    """
    serializer_class = BizuthSerializer
    def get_queryset(self):
        queryset = Bizuth.objects.filter(actif=True)
        bus = self.request.query_params.get('bus')
        if bus=='null':
            print('aucun bus')
            queryset = queryset.filter(bus=None)
        elif bus is not None:
            try:
                if isinstance(int(bus), int):
                    queryset = queryset.filter(bus=bus)
            except ValueError:pass
        return queryset
    permission_classes = [IsOrga]
    lookup_field = 'adhesion_id' #utilise l'ID Adhésion comme pk

    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['_nom', '_prenom', '_email']
    filter_fields = ['_a_paye', 'caution_ok', 'parents_ok', 'bungalow','_valide']

    @action(methods=['get'], detail=True)
    def adhesion(self, request, adhesion_id): #et pas 'pk'
        """
        Mis à jour
        """
        biz = self.get_object()
        biz.adhesion_data = ""
        biz.email= ""
        biz.save() #sinon ça va créer
        return Response(biz.adhesion)
        pass

    @action(detail=False)
    def attente(self, request):
        """
        renvoie la liste d'attente
        gaffe, ça va effectuer des grosses requêtes SQL (update sur tous les bizuths)
        """
        limite = settings.PLACES_MAX
        queryset = self.get_queryset().order_by('date_inscription','id') #changer si on modifie le queryset de base

        liste_valide  =queryset[:int(limite)]
        for biz in liste_valide:
            biz.place_attente = 0
            biz.save()

        liste_attente = queryset[int(limite):]
        liste, i = [], 1
        for biz in liste_attente: #TODO: filtrer selon si ils ont payé, donné le chèque de caution ??? ...
            biz.place_attente = i
            biz.save()
            liste.append(biz)
            i+=1 #TODO: remplacer ça par un itérateur pythonique
        return Response(BizuthSerializer(liste, many=True).data)

    @action(detail=False)
    def readliste(self, request):
        """
        renvoie simplement la liste d'attente
        """
        liste = self.get_queryset().exclude(place_attente=0).exclude(place_attente=-1).order_by('place_attente').all()
        return Response(BizuthSerializer(liste, many=True).data)

    @action(detail=True, methods=['post'])
    def paye(self, request,adhesion_id=None):
        biz = Bizuth.objects.get(adhesion_id=adhesion_id)
        biz.a_paye=True
        biz.save()
        return Response(BizuthSerializer(biz).data)


class BusViewSet(viewsets.ModelViewSet):
    """
    L'enpoint /bus/{id}/bizuths/ renvoie tous les bizuths dans ce bus
    Accès public aux noms des bus & places (mais pas aux gens dedans)
    """
    serializer_class = BusSerializer
    queryset = Bus.objects.all()
    permission_classes = [IsOrgaOrReadOnly]

    @action(methods=['get'], detail=True)
    def bizuths(self, request, pk):
        if self.request.user.is_authenticated:
            if not self.request.user.is_staff:
                raise PermissionDenied()
        else: raise NotAuthenticated()
        bus = Bus.objects.get(id=pk)
        bizuths = bus.bizuths.exclude(actif=False)
        serializer = BizuthSerializer(bizuths, many=True)
        return Response(serializer.data)

class BungalowViewset(viewsets.ModelViewSet):
    """
       L'enpoint /bungalows/{id}/bizuths/ renvoie tous les bizuths dans ce bungalow
    """
    serializer_class = BungalowSerializer
    queryset = Bungalow.objects.all()
    permission_classes = [IsOrga]
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['bus']
    @action(methods=['get'], detail=True)
    def bizuths(self, request, pk):
        return Response(BizuthSerializer(self.get_object().bizuths.exclude(actif=False), many=True).data) # voir au-dessus, le code est plus découpé

@api_view()
def me(request):
    return Response(request.user.username+ " "+str(request.user.is_staff))

@api_view()
def get_refresh_token(request):
    return Response(refresh_token(request))

@api_view()
@permission_classes([IsStaff])
def admin_stats_view(request):
    bizuths = Bizuth.objects.filter(actif=True)
    bus_remplis=0
    taux_bus, taux_bungalows = {}, {}
    places_occupees_total = 0
    places_total = 0
    file_attente = 0
    incomplet = 0
    participants_valide = 0
    for obj in Bus.objects.all():
        places_occupees_total += obj.places_occupees
        places_total += obj.places
        try:taux_bus[obj.nom] = obj.places_occupees/obj.places
        except ZeroDivisionError: taux_bus[obj.nom]=0
        if obj.places_restantes<=0:
            bus_remplis+=1
    try: taux_bus_total = places_occupees_total/places_total
    except ZeroDivisionError: taux_bus_total =0
    places_occupees_total = 0
    places_total = 0
    bungalows_remplis=0
    for obj in Bungalow.objects.all():
        places_occupees_total += obj.places_occupees
        places_total += obj.places
        try:taux_bungalows[obj.nom] = obj.places_occupees/obj.places
        except ZeroDivisionError: taux_bungalows[obj.nom]=0
        if obj.places_restantes<=0:
            bungalows_remplis+=1
    try: taux_bungalows_total = places_occupees_total/places_total
    except ZeroDivisionError: taux_bungalows_total =0
    mineurs = 0
    autorisations_parentales_manquantes=0
    for obj in bizuths:
        if obj.est_mineur:
            mineurs+=1
            if not obj.parents_ok:
                autorisations_parentales_manquantes+=1
                #TODO faire le tri dans les champs utile ou non
    response = {
        'bus':Bus.objects.all().count(),
        'bus_remplis': bus_remplis,
        'bungalows':Bungalow.objects.all().count(),
        'bungalows_remplis':bungalows_remplis,
        'bizuths': bizuths.count(),
        'inscription_valide': bizuths.filter(_valide=True).count(),
        'inscription_valide_bus': bizuths.filter(_valide=True).exclude(bus=None).count(),
        'participants_valide': bizuths.filter(_valide=True,place_attente=0).count(),
        'participants_invalides': bizuths.filter(_valide=False,place_attente=0).count(),
        'places_max': settings.PLACES_MAX,
        'inscrit_sans_bus':  bizuths.filter(_valide=True,bus=None).count(),
        'file_attente': bizuths.exclude(place_attente=0).count(),
        'mineurs':mineurs,
        'nombre_fille': bizuths.filter(_genre='F').count(),
        'autorisations_parentales_manquantes':autorisations_parentales_manquantes,
        'cautions_manquantes': Bizuth.objects.filter(caution_ok=False).count(),
        'paiements': bizuths.filter(_a_paye=True).count(),
        'desinscrits': BizuthSerializer(Bizuth.objects.filter(actif=False), many=True).data,
        'taux_bus_total': taux_bus_total*100,
        'taux_bungalows_total': taux_bungalows_total*100,
        'taux_bus': [{'nom':k,'taux':100*v} for k, v in taux_bus.items()],
        'taux_bungalows': [{'nom':k, 'taux':100*v} for k, v in taux_bungalows.items()]

    }
    return Response(response)

#@api_view()
def status(request):
    try:user=str(request.user)
    except: user = None
    try:
        api = AdhesionAPI()
        api.get_member(1)
        adhesion_ok = True
    except: adhesion_ok=False
    return HttpResponse(json.dumps({
        'adhesion': adhesion_ok,
        'user': user #sera AnonymousUser et pas null; sauf erreur
    }))


@permission_classes([IsStaff])
def export_data(request):
    """
    Cette vue propose plusieurs formats d'export, à définir avec http://export/?format=xlsx
    \nPar défaut c'est du CSV
    :param request:
    :return:
    """
    format = request.GET.get('ext', 'xls')
    mimetypes = {
        'csv':'text/csv',
        'ods': 'application/vnd.oasis.opendocument.spreadsheet',
        'xls':'application/vnd.ms-excel',
        'xlsx':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'json':'application/json'
    }
    queryset = Bizuth.objects.filter(actif=True)
    data = tablib.Dataset()
    data.headers = ['Prénom', 'Nom', 'E-mail', 'Téléphone', 'Date de Naissance', 'Mineur',
                      'Autorisation Parentale', "Date d'inscription", 'Caution donnée',
                      "Liste d'attente", 'Bus', 'Bungalow']
    for b in queryset:
        data.append([b.prenom, b.nom, b.email, b.telephone, b.date_naissance, b.est_mineur, b.autorise,
                       b.date_inscription, b.caution_ok, b.place_attente, str(b.bus) if b.bus else '',
                     str(b.bungalow) if b.bungalow else ''])
    response = HttpResponse(data.export(format), content_type=mimetypes[format])
    if format != 'json':
        response['Content-Disposition'] = 'attachment; filename="Wei-export-bizuths.'+format+'"'
    return response

@permission_classes([IsStaff])
def export_inscrit(request):
    """
    Cette vue propose plusieurs formats d'export, à définir avec http://export/?format=xlsx
    \nPar défaut c'est du CSV
    :param request:
    :return:
    """
    format = request.GET.get('ext', 'xls')
    mimetypes = {
        'csv':'text/csv',
        'ods': 'application/vnd.oasis.opendocument.spreadsheet',
        'xls':'application/vnd.ms-excel',
        'xlsx':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'json':'application/json'
    }
    queryset = Bizuth.objects.filter(actif=True,place_attente=0)
    data = tablib.Dataset()
    data.headers = ['Prénom', 'Nom', 'E-mail', 'Téléphone', 'Date de Naissance', 'Mineur',
                      'Autorisation Parentale', "Date d'inscription", 'Caution donnée',
                      "Liste d'attente", 'Bus', 'Bungalow']
    for b in queryset:
        data.append([b.prenom, b.nom, b.email, b.telephone, b.date_naissance, b.est_mineur, b.autorise,
                       b.date_inscription, b.caution_ok, b.place_attente, str(b.bus) if b.bus else '',
                     str(b.bungalow) if b.bungalow else ''])
    response = HttpResponse(data.export(format), content_type=mimetypes[format])
    if format != 'json':
        response['Content-Disposition'] = 'attachment; filename="Wei-export-bizuths.'+format+'"'
    return response

@permission_classes([IsStaff])
def export_sante(request):
    """
    Cette vue propose plusieurs formats d'export, à définir avec http://export/?format=xlsx
    \nPar défaut c'est du CSV
    :param request:
    :return:
    """
    format = request.GET.get('ext', 'xls')
    mimetypes = {
        'csv':'text/csv',
        'ods': 'application/vnd.oasis.opendocument.spreadsheet',
        'xls':'application/vnd.ms-excel',
        'xlsx':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'json':'application/json'
    }
    queryset = Bizuth.objects.filter(actif=True,sante__isnull=False,place_attente=0).exclude(sante__exact="")
    data = tablib.Dataset()
    data.headers = ['Prénom', 'Nom', 'Téléphone', 'Mineur',
                     'Bus', 'Details de santé']
    for b in queryset:
        data.append([b.prenom, b.nom, b.telephone, b.est_mineur, str(b.bus) if b.bus else '',
                     b.sante])
    response = HttpResponse(data.export(format), content_type=mimetypes[format])
    if format != 'json':
        response['Content-Disposition'] = 'attachment; filename="Wei-export-bizuths.'+format+'"'
    return response

