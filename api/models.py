import datetime
import logging

from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.dateparse import parse_date
from django.utils.datetime_safe import date #from datetime import date
from rest_framework.exceptions import APIException

from api.adhesion_api import AdhesionAPI
import  json

from wei import settings

UserModel = get_user_model()
# Create your models here.

logger = logging.getLogger("api")


class Bus(models.Model):
    nom = models.CharField(max_length=255, verbose_name="Nom du Bus")
    places = models.IntegerField(verbose_name="Nombre total de places")
    def __str__(self):
        return self.nom
    @property
    def places_occupees(self):
        return self.bizuths.count()
    @property
    def places_restantes(self):
        return self.places - self.places_occupees

GENRES = [
    ('H', "Homme"),
    ('F', "Femme"),
    ('I', 'Indéfini/Inconnu')
]
class Bungalow(models.Model):
    nom = models.CharField(verbose_name="Nom du bungalow", max_length=255)
    genre = models.CharField(max_length=1, choices=GENRES)
    places = models.IntegerField(verbose_name="Nombre de places")
    bus = models.ForeignKey(to=Bus, null=True, on_delete=models.PROTECT)
    def __str__(self):
        return self.nom
    @property
    def places_occupees(self):
        return self.bizuths.count()
    @property
    def places_restantes(self):
        return self.places - self.places_occupees

class Bizuth(models.Model):
    adhesion_id = models.IntegerField(verbose_name='ID du membre sur Adhésion', unique=True, null=False)
    _nom = models.CharField(verbose_name='Nom du bizuth', blank=True, max_length=255, null=True)
    _prenom = models.CharField(verbose_name='Prénom du bizuth', blank=True, max_length=255,null=True)
    _email = models.EmailField(verbose_name='Email du bizuth', blank=True, max_length=255,null=True)
    _genre = models.CharField(max_length=1, choices=GENRES, default='I')
    adhesion_data = models.CharField(verbose_name="Données d'Adhésion mises en cache", max_length=4095, blank=True)
    date_inscription = models.DateField(verbose_name="Date d'inscription", default=date.today, blank=True)

    parents_ok = models.BooleanField(verbose_name='Autorisation parentale')
    # user = models.ForeignKey(to=UserModel, verbose_name="Association à un utilisateur", null=True)
    caution_ok = models.BooleanField(verbose_name="Chèque de caution", default=False)
    _a_paye = models.BooleanField(verbose_name="A payé", default=False)
    actif = models.BooleanField(verbose_name="Inscription active", default=True,
                                help_text="N'a pas annulé son inscription")
    _valide = models.BooleanField(verbose_name="Inscription Valide", default=False)
    details = models.TextField(verbose_name="Détails", blank=True)
    sante = models.TextField(verbose_name="Précisions médicales", blank=True)
    bus = models.ForeignKey(to=Bus, null=True, on_delete=models.PROTECT, related_name='bizuths', blank=True)
    #bungalow = models.ForeignKey(to=Bungalow, null=True, on_delete=models.PROTECT, related_name='bizuths', blank=True)
    bungalow = models.TextField(null=True,verbose_name="Bungalow",max_length=5,blank=True)

    place_attente = models.IntegerField(verbose_name="Place dans la file d'attente (0 si pas dedans)", null=True, default=-1)


    def __str__(self):
        return "#{0} {1} {2}".format(self.pk, self.prenom, self.nom)

    @property
    def adhesion(self): #dictionnaire (nested) contenant les données du membre sur Adhésion
        if self.adhesion_data == "":
            if self.adhesion_id is None: return None
            try:
                api = AdhesionAPI()
                adhesion = api.get_member(self.adhesion_id)
                self.adhesion_data = json.dumps(adhesion)
                if self.pk:self.save()
                if api.try_member(self.adhesion_id):
                    adhesion = api.get_member(self.adhesion_id)
                    self.adhesion_data = json.dumps(adhesion)
                    self.save()
                    return adhesion
                else: #le membre n'existe pas
                    self.adhesion_id = None
                    self.adhesion_data = ""
                    self.save()
                    adhesion = None
            except:
                logger.exception("Impossible de charger les données Adhésion pour le bizuth id {}".format(self.adhesion_id))
                return None
        else:
            adhesion = json.loads(self.adhesion_data)
        return adhesion

    @property
    def nom(self):
        if self._nom is not None and self._nom != "": return self._nom
        elif self.adhesion_id is not None:
            self.nom = self.adhesion.get('last_name')
            if self.pk: self.save()
            return self.nom
    @nom.setter
    def nom(self, nom):
        self._nom = nom

    @property
    def prenom(self):
        if self._prenom != "" and self._prenom is not None: return self._prenom
        elif self.adhesion_id is not None:
            logger.debug("df"+str(self.adhesion_id))
            self._prenom = self.adhesion.get('first_name')
            if self.pk:self.save()
            return self.prenom
    @prenom.setter
    def prenom(self, prenom):
        self._prenom = prenom

    @property
    def email(self):
        if self._email is not None and self._email != "": return self._email
        else:
            self.email = self.adhesion.get('email')
            if self.pk: self.save()
            return self.email
    @email.setter
    def email(self, email):
        self._email = email
        if self.pk: self.save()
    @property
    def telephone(self):
        return self.adhesion.get('phone')

    @property
    def est_mineur(self):
        if self.adhesion is None: return False #

        a = self.adhesion.get('birthday')
        if a is None: return False
        anniv = parse_date(a)
        date_wei = parse_date(settings.DATE_WEI)
        anniv_minimal = datetime.date(year=date_wei.year-18,
                                 month=date_wei.month,
                                 day=date_wei.day)
        if anniv > anniv_minimal:
            return True
        else:return False
    @property
    def autorise(self):
        """
        Vérifie que le biz a une autorisation parentale s'il est mineur
        ??????
        """
        if self.est_mineur:
            if self.parents_ok: return True
            else: return False
        else:
            self.parents_ok = True
            if self.pk:self.save()
            return True
    @autorise.setter
    def autorise(self, autorise):
        self.parents_ok = autorise
        if self.pk: self.save()
    @property
    def valide(self):
        if self.autorise and self.a_paye and self.caution_ok:
            self._valide = True
            self.save()
            return True
        else:
            self._valide = False
            self.save()
            return False

    @property
    def inscrit(self):
        """
        Renvoie True si le bizuth n'est pas dans la file d'attente et qu"il est validé (payé, autorisé ou majeur)
        """
        if self.valide and self.place_attente == 0 and self.actif:
            return True
        else: return False

    @property
    def genre(self):
        if self._genre == "I" or self._genre is None:
            if self.adhesion.get('gender') == 'M':
                self._genre = "H"
            elif self.adhesion.get('gender') == 'W':
                self._genre = "F"
            if self.pk: self.save()
        else:
            return self._genre
    @property
    def a_paye(self):
        """
        Oui je désactive ton code, car sinon il fait 25 000 requetes
        """
        return self._a_paye
        #if self._a_paye:
        #    return True
        #else:
        #    api = AdhesionAPI()
        #    paiements = api.get_paiements(adhesion_id=self.adhesion_id, raison='wei')
        #    if len(paiements) == 1:
        #        self._a_paye = True
        #        self.save()
        #        return True
        #    elif len(paiements) > 1:
        #        raise APIException({'detail':"Cas étrange : plus d'un paiement pour le wei",
        #                            'bizuth':self,
        #                            'paiements':paiements,
        #                            })
        #    return False

    @a_paye.setter
    def a_paye(self, a_paye):
        self._a_paye = a_paye

    @genre.setter
    def genre(self, genre):
        self._genre = genre
        if self.pk: self.save()

    @property
    def date_naissance(self):
        if self.adhesion is None: return None
        return self.adhesion.get('birthday')