# Generated by Django 2.0.6 on 2018-09-13 19:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20180912_1523'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bizuth',
            name='place_attente',
            field=models.IntegerField(default=-1, null=True, verbose_name="Place dans la file d'attente (0 si pas dedans)"),
        ),
    ]
