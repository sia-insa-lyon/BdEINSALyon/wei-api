from django.core.management.base import BaseCommand, CommandError
from wei import settings
from api.models import Bizuth

class Command(BaseCommand):
    help = 'Rafraichit la liste d\'attente'

    def handle(self, *args, **options):
        """
        rafraichit la liste d'attente
        gaffe, ça va effectuer des grosses requêtes SQL (update sur tous les bizuths)
        """
        limite = settings.PLACES_MAX
        bizuths = Bizuth.objects.filter(actif=True).order_by('date_inscription','id')

        liste_valide = bizuths[:int(limite)]
        for biz in liste_valide:
            biz.place_attente = 0
            biz.save()

        liste_attente = bizuths[int(limite):]
        liste, i = [], 1
        for biz in liste_attente:  # TODO: filtrer selon si ils ont payé, donné le chèque de caution ??? ...
            biz.place_attente = i
            biz.save()
            liste.append(biz)
            i += 1  # TODO: remplacer ça par un itérateur pythonique
        self.stdout.write(str(liste), ending='')
