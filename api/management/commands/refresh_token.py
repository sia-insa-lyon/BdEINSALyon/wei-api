from django.core.management.base import BaseCommand, CommandError
from account.custom import get_new_token

class Command(BaseCommand):
    help = 'Rafraichit le token'

    def handle(self, *args, **options):
        get_new_token()
        print("GOT NEW TOKEN")
        self.stdout.write("refreshed")
