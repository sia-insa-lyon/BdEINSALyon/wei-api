from django.urls import path
from rest_framework.routers import DefaultRouter
from api import views
from account import custom

router = DefaultRouter()
router.register('bizuths', views.BizuthViewSet, 'bizuths')
router.register('bus', views.BusViewSet)
router.register('bungalows', views.BungalowViewset)

urlpatterns = [
    path('me', views.me),
    path('stats/', views.admin_stats_view),
    path('status/', views.status),
    path('export/', views.export_data),
    path('export_sante/', views.export_sante),
    path('export_inscrit/', views.export_inscrit),
    path('refresh_token/', custom.refresh_token),
]+router.urls