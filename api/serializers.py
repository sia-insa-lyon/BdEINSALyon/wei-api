from rest_framework import serializers

from api.models import *


class BizuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bizuth
        exclude = ['_nom', '_prenom', '_email', '_genre' ,'parents_ok', 'adhesion_data', 'id', '_a_paye','_valide']
    nom = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    prenom = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    email = serializers.EmailField(required=False, allow_blank=True, allow_null=True)
    telephone = serializers.CharField(read_only=True)
    est_mineur = serializers.BooleanField(read_only=True)
    valide = serializers.BooleanField(read_only=True, default=False,help_text="Payé et autorisé", label="Validé/Inscrit")
    inscrit = serializers.BooleanField(read_only=True, help_text="False si il est en liste d'attente", label="Inscrit")

    autorise = serializers.BooleanField(default=False, help_text="Autorisation parentale s'il est mineur",
                                        label="Autorisé")
    genre = serializers.CharField(read_only=True, help_text="H:homme, F:femme, I:inconnu")
    #a_paye = serializers.BooleanField(read_only=True,help_text="Read-Only: dis si l'utilisateur a payé pour le wei")
    a_paye = serializers.BooleanField(help_text="Communique avec Paiements pour savoir si l'utilisateur a payé pour le wei")

    def validate(self, data):
        adhesion_id = data.get('adhesion_id', None)
        if adhesion_id is not None and adhesion_id !=0:
            try:
                api = AdhesionAPI()
                if not api.try_member(adhesion_id):
                    raise serializers.ValidationError("")#c'est pour différencier les erreurs
            except serializers.ValidationError:         #réseau des erreurs
                raise serializers.ValidationError("Membre inexistant") #d'inexistence
            except:
                logger.exception('erreur inconnu')
                raise serializers.ValidationError("Erreur Adhésion ou (moins probable), membre inexistant")
        try:
            if self.instance.bus is not None and data.get('bus', None) is not None: #si on le change de bus
                if data.get('bus') != self.instance.bus and data.get('bus').places_restantes <=0:#si on le remet pas dans le même
                    raise serializers.ValidationError("Il n'y a plus de place dans ce bus")
                else : pass #il reste dans le même bus
            elif self.instance.bus is None and data.get('bus', None) is not None: #si on le met dans un bus
                if data.get('bus').places_restantes <= 0:
                    raise serializers.ValidationError("Il n'y a plus de place dans ce bus")
            else: pass #il n'y a pas de bus défini
        except AttributeError: #self.instance n'existe pas, le Bizuth vient d'être crée
            try:
                if data.get('bus').places_restantes <= 0:
                    raise serializers.ValidationError("Il n'y a plus de place dans ce bus")
            except AttributeError:#si aucun bus n'est défini pour ce bizuth
                pass

        try:
            if self.instance.bus is not None and data.get('bus', None) is not None: #si on le change de bus
                if data.get('bungalow') != self.instance.bungalow and data.get('bungalow').places_restantes <=0:#si on le remet pas dans le même
                    raise serializers.ValidationError("Il n'y a plus de place dans ce bungalow")
            elif self.instance.bungalow is None and data.get('bungalow', None) is not None: #si on le met dans un bus
                if data.get('bungalow').places_restantes <= 0:
                    raise serializers.ValidationError("Il n'y a plus de place dans ce bungalow")
        except AttributeError:
            try:
                if data.get('bungalow').places_restantes <= 0:
                    raise serializers.ValidationError("Il n'y a plus de place dans ce bungalow")
            except AttributeError: #pas de Bungalow défini
                pass
        return super().validate(data)

class BusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bus
        fields = '__all__'
    places_restantes = serializers.IntegerField(read_only=True)
    places_occupees = serializers.IntegerField(read_only=True)

class BungalowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bungalow
        fields = '__all__'
    places_restantes = serializers.IntegerField(read_only=True)
    places_occupees = serializers.IntegerField(read_only=True)
