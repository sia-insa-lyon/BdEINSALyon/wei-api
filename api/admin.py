from django.contrib import admin
from api.models import *
# Register your models here.
@admin.register(Bungalow, Bus)
class Admin(admin.ModelAdmin):
    pass

@admin.register(Bizuth)
class BizuthAdmin(admin.ModelAdmin):
    list_display = ('adhesion_id','genre', 'nom', 'prenom', 'email','est_mineur', 'bus','bungalow','inscrit')
    search_fields = ('adhesion_id', 'nom', 'prenom')
    list_filter = ('_genre','bus')
    list_per_page = 25

