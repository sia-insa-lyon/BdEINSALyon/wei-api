# WEI API
Backend pour gérer les inscription au WEI, les bus ...

## Déploiement
Variables d'environnements:
```
ADHESION_CLIENT_ID=UAuHBWOjx5Oj9gyWmLiPQ8at9wqEyseTkHlzEktr
ADHESION_CLIENT_SECRET=bDXGjVuw4YGRBSLE2hiAZBpJ3mhI0h7qB0ESWskWB9WeUzrdF2h1U53JGxNO7wrkW4Tpgc3gcuH5D0EJpt3CIEmNuhCKBQeEk5bt8udKZBhtcjNFH1AsDo8rKmUXEdmZ
ADHESION_URL=http://localhost:8000
DEBUG=True
ADHESION_COOLDOWN=30 #temps en minutes où l'utilisateur est mis en cache dans WEI
DATE_WEI=2018-09-01 #la date à partir de laquelle les gens doivent être majeurs
OVERRIDE_PERMISSIONS=False #pour désactiver le besoin de s'authentifier (permission IsOrga)
PLACES_MAX=460 #nombre de places (pour savoir où couper pour la file d'attente)
```
NOTE: à cause d'une initialisation bizarre, WEI va demander un token à Adhésion même si on lance `manage.py migrate`
Il faut donc avoir une configuration correcte (même si Adhésion n'est pas joignable)
Cependant c'est plutôt bien car j'aurais pas trouvé comment recevoir un token au démarrage
## Setup développement
Pour accéder aux endpoints, il faut être staff (votre compte root sur Adhésion marchera)
* les requêtes OAuth sont interdites en HTTPS. Pour changer ça il faut mettre la variable d'environnement
`OAUTHLIB_INSECURE_TRANSPORT=1`
* Il faut créer sur [Adhésion](http://localhost:8000/o/applications) une Application OAuth de type confidential, Client Credentials

NOTE: OAuth autorise toutes les requêtes en mode DEBUG - changez ça dans `custom.py`
## Fonctionnement
WEI est un Ressource Server qui délègue l'authentification à Adhésion
Il utilise un token Bearer pour vérifier les authenfitication aupres de Adhésion.
il faut générer un ACCESS TOKEN qui expire dans *très longtemps* dans Adhésion (voir interface admin), sinon y'aura des erreurs

## Tester les vues protégés
Il faut créer une Application pour vous (type client-credentials c'est plus simple), et demander un token à adhésion
```
curl -X POST -d "grant_type=client_credentials" -u"CLIENT_ID:CLIENT_SECRET" http://localhost:8000/o/token/
```
et ensuite faire les requêtes avec le access_token reçu :
```
curl -H "Authorization: Bearer ACCESS_TOKEN" http://localhost:8001/account/me
```
NOTE : en mode DEBUG, les View ne demandent pas d'authentification OAuth si on utilise
le @protected_ressource de custom.py

```
© 2018 BdE INSA Lyon
© 2018 Jean RIBES
```
