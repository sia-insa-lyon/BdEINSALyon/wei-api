import os, django
from wei import settings
if __name__=='__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "wei.settings")
    django.setup()
    from api.models import Bizuth, Bus, Bungalow

    les_bus = Bus.objects.all()
    print(les_bus)
    bungalows_meuf = [obj for obj in Bungalow.objects.filter(genre='F') if obj.places_restantes > 0] #pas de filter car c'est pas dans la DB
    bungalows_mec = [obj for obj in Bungalow.objects.filter(genre='H') if obj.places_restantes > 0]
    print(bungalows_mec)
    i_Bmeuf, i_Bmec= 0,0
    for bus in les_bus: #c'est pour que les gens dans le même bus soient groupés en bungalows
    #for biz in bus.bizuths.filter(actif=True, parents_ok=True, place_attente=0)
        print(bus.nom)
        print(bus.bizuths.filter(actif=True))
        for biz in bus.bizuths.filter(actif=True):
            print("---| [{}] > {} {}".format(bus.nom, biz.prenom, biz.nom))
            if biz.genre == "F":
                B = bungalows_meuf[i_Bmeuf]
                if B.places_restantes>0:
                    biz.bungalow = B
                    #biz.save()
                    print("------| rajouté {0} {1} dans le bungalow {2} ({3})".format(biz.prenom, biz.nom, B.nom, biz.bungalow.nom))
                else:
                    i_Bmeuf+=1 #on passe au bungalow meuf suivant (ça remplit les trous dans les bus)

            elif biz.genre == "H":
                B = bungalows_mec[i_Bmec]
                if B.places_restantes>0:
                    biz.bungalow = B
                    #biz.save()
                    print("rajouté {0} {1} dans le bungalow {2} ({3})".format(biz.prenom, biz.nom, B.nom, biz.bungalow.nom))
                else:
                    i_Bmec+=1

            else: pass #pas de lits pour les non-genrés :);et il faudra gérer au cas par cas les bizuths restants
