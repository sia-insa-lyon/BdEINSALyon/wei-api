FROM python:3.5

EXPOSE 8000
ENV LIBRARY_PATH=/lib:/usr/lib
WORKDIR /app
COPY . /app
RUN pip install gunicorn && pip install -r /app/requirements.txt
RUN chmod +x scripts/run-prod.sh
CMD /app/scripts/run-prod.sh